const todosRouter = require('express').Router()
const jwt = require('jsonwebtoken')
const Todo = require('../models/todo')
const User = require('../models/user')

const getTokenFrom = (request) => {
  const authorization = request.get('authorization')
  if (authorization && authorization.toLowerCase().startsWith('bearer ')) {
    return authorization.substring(7)
  }
  return null
}
todosRouter.get('/', async (request, response) => {
  const todos = await Todo.find({}).populate('user', { username: 1, name: 1 })
  response.json(todos)
})

todosRouter.get('/:id', async (request, response) => {
  const todo = await Todo.findById(request.params.id)
  if (todo) {
    response.json(todo)
  } else {
    response.statusMessage = 'No todo!' // status message shows on console
    response.status(404).send('This todo does not exist.')
  }
})

todosRouter.post('/', async (request, response) => {
  const body = request.body
  const token = getTokenFrom(request)

  const decodedToken = jwt.verify(token, process.env.SECRET)
  if (!decodedToken.id) {
    return response.status(401).json({ error: 'token missing or invalid' })
  }
  const user = await User.findById(decodedToken.id)

  const todo = new Todo({
    title: body.title,
    important: body.important || false,
    date: new Date(),
    done: false,
    user: user._id,
  })

  const savedTodo = await todo.save()
  user.todos = user.todos.concat(savedTodo._id)
  await user.save()

  response.status(201).json(savedTodo)
})

todosRouter.put('/:id', (request, response, next) => {
  const body = request.body

  const todo = {
    title: body.title,
    important: body.important,
  }

  Todo.findByIdAndUpdate(request.params.id, todo, {
    new: true,
    // validations arenot run by default when findOneAndUpdate is executed
    runValidators: true,
    context: 'query',
  })
    .then((updatedTodo) => {
      response.json(updatedTodo)
    })
    .catch((error) => next(error))
})

todosRouter.delete('/:id', async (request, response) => {
  await Todo.findByIdAndRemove(request.params.id)
  response.status(204).end()
})

module.exports = todosRouter
