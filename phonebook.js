const mongoose = require("mongoose");

// A cloud-based MongoDB phonebook database
if (process.argv.length < 3) {
  console.log("Enter your password!!");
}
const password = process.argv[2];
const url = `mongodb+srv://fullstackopen:${password}@cluster0.068kc.mongodb.net/PhoneBook-DB?retryWrites=true&w=majority`;

const personSchema = new mongoose.Schema({
  name: String,
  number: String,
});

const Person = mongoose.model("Person", personSchema);

if (process.argv.length > 3) {
  const name = process.argv[3];
  const number = process.argv[4];

  mongoose
    .connect(url)
    .then((result) => {
      console.log("connected to database");

      const person = new Person({
        name: name,
        number: number,
      });
      return person.save();
    })
    .then(() => {
      console.log("added", name, "number", number, "to phonebook");
      return mongoose.connection.close();
    })
    .catch((err) => console.log(err));
} else if (process.argv.length === 3) {
  mongoose.connect(url);
  Person.find({}).then((result) => {
    console.log("phonebook");
    result.forEach((person) => console.log(person.name, person.number));
    mongoose.connection.close();
  });
}
