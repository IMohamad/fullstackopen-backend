const Todo = require('../models/todo')
const User = require('../models/user')

const initialTodos = [
  {
    title: 'Learn how to use websockets',
    date: new Date(),
    important: true,
  },
  {
    title: 'Learn how to write schemas for MongDB',
    date: new Date(),
    important: true,
  },
]

const nonExistingId = async () => {
  const todo = new Todo({ title: 'will remove this soon', date: new Date() })
  await todo.save()
  await todo.remove()

  return todo._id.toString()
}

const todosInDb = async () => {
  const todos = await Todo.find({})
  return todos.map((todo) => todo.toJSON())
}

const usersInDb = async () => {
  const users = await User.find({})
  return users.map((u) => u.toJSON())
}

module.exports = {
  initialTodos,
  nonExistingId,
  todosInDb,
  usersInDb,
}
